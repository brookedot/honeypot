# Honey Pot Project Lookup

## Description
A PHP library for querying the [Project Honeypot Http:BL API](http://www.projecthoneypot.org/httpbl_api.php) based on [Project Honeypot Http:BL Library](https://github.com/thasmo/php.honeypot-blacklist).

## Usage

### Create a new instance
```php
require_once 'honeypot.class.php';
use HoneyPot\Lookup;

Lookup::setDefaultKey('api-key');
$client = new Lookup('127.0.0.1');
```

### Methods
```php

# Manually set the API key during the lookup
$client = new Lookup('127.0.0.1', 'api-key');

# Client is a search engine.
$client->isSearchEngine();

# Client is suspicious.
$client->isSuspicious();

# Client is a harvester.
$client->isHarvester()

# Client is a spammer.
$client->isSpammer();

# Client is blacklisted.
# Which means it is suspicious, a harvester or a spammer but not a search engine.
$client->isListed();

# Get the last activity for the client in days.
$lastActivity = $client->getActivity(); 

# Get the threat score of the client.
$threatScore = $client->getThreat();

# Check if the client was active in the last 10 days.
$isActive = $client->isActive(10);

# Check if the threat score is within the limit of 100.
$isThreat = $client->isThreat(100);

# Get the name of the search engine.
if($client->isSearchEngine()) {
  $name = $client->getName();
}

# Return an array holding the result from the API call
$result = $client->getResult();

# Query the API immediately
$client->query();
# Use other methods
if($client->isSearchEngine()) {
  $name = $client->getName();
}
```

## Implementation Details

* Requests to the API are delayed until you first call a method like `isSearchEngine` etc. or `query` explicitly.
* API responses for the same IP address on the same instance will be cached, the API will be queried only once.
* When changing the IP address via `setAddress` the cache is cleared and the API will be queried again.

## Changelog

1.0 2022-11-23*
 - Initial release

## Authors and acknowledgment

(c) 2022 Brooke.

This is a modification of [Project Honeypot Http:BL Library](https://github.com/thasmo/php.honeypot-blacklist) (c) 2015 Thomas "Thasmo" Deinhamer 

The following Changinges have been made:
 * Updated Coding Standards to PSR12 Standards
 * Changed Namespace from Thasmo\ProjectHoneypot to Honeypot\Lookup
 * Changed "Blacklist" to "Lookup" due to the history of the term "Blacklist" `blacklist.php` is now `honeypot_class.php`
 * Added "class exsist" check to pevent class from being added twice
 * Removes `InvalidArgumentException` and uses `trigger_error` instead so the script doesn't fatal on bad IP or API Key
 * A few minor changes to the class

## License
 Code released under the [MIT License](https://gitlab.com/brookedot/honeypot/-/blob/main/).
